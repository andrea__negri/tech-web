<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("HTTP/1.0 500 User non loggato");
    exit;
} else {
    if(!isset($_POST["numeroBiglietti"]) || !isset($_POST["idEvento"])){
        header("HTTP/1.0 500 Parametri non validi");
        exit;
    } else {
        $userData["numeroBiglietti"] = $_POST["numeroBiglietti"];
        $userData["idEvento"] = $_POST["idEvento"];
        $userData["idUser"] = getUserId();
        $userData["idCarrello"] = getCartId();
        if($dbh->addToCart($userData)){
            header("HTTP/1.0 200 Aggiunti al carrello");
            exit;
        } else {
            header("HTTP/1.0 500 Errore durante l'inserimento");
            exit;
        }
    }
}
?>