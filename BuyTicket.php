<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("HTTP/1.0 500 User non loggato");
    exit;
} else {
    if(!isset($_POST["proprietario"]) || !isset($_POST["carta"]) || !isset($_POST["cvv"])){
        header("HTTP/1.0 500 Parametri non validi");
        exit;
    } else {
        $userData["proprietario"] = $_POST["proprietario"];
        $userData["carta"] = $_POST["carta"];
        $userData["cvv"] = $_POST["cvv"];
        $userData["data"] = date("Y-m-d-G:i:s", time());
        $userData["id"] = getUserId();
        $userData["idCarrello"] = getCartId();
        if($dbh->insertOrder($userData)){
                $dbh->updateSoldTickets();
                header("HTTP/1.0 200 Ok");
                require 'index.php';
                exit;
        } else {
            header("HTTP/1.0 500 Errore durante l'ordine");
            exit;
        }
    }
}
?>