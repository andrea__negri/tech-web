<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    require 'LoginForm.php';
} else {
    $templateParams["biglietti"] = $dbh->getCart(getCartId());
    require 'CartDetail.php';
}
?>