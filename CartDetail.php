<?php if(!isset($_SESSION)){session_start();}ob_start();?>
<!DOCTYPE html>
<html lang="it">
<head>
<?php require 'template/head.php'; ?>
<link rel="stylesheet" href="css/cartDetail.css">
<script src="js/cart.js"></script>
</head>
<body>
    <?php require 'template/nav.php'; ?>
    <div class="container">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th class="w-50">Biglietti</th>
                    <th class="w-10">Prezzo</th>
                    <th class="w-8">Quantità</th>
                    <th class="text-center w-22">Sub-totale</th>
                    <th class="w-10"></th>
                </tr>
            </thead>
            <tbody>
            <?php $tot=0; foreach($templateParams["biglietti"] as $biglietto): ?> 
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs"><img src=<?php echo IMG_DIR.$biglietto["urlFoto"]?> alt="..."class="img-responsive img-size" /></div>
                            <div class="col-sm-10">
                                <h4 class="nomargin"><a href="<?php echo "Detail.php?id=".$biglietto["idEvento"]?>"><?php echo $biglietto["nome"]?></a></h4>
                                <p><?php echo $biglietto["descrizione"]?></p>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">€<?php echo $biglietto["prezzo"]?></td>
                    <td data-th="Quantity">
                        <input id="evento-<?php echo $biglietto["idEvento"]?>" type="number" onChange="modifyQty(<?php echo ($biglietto["idEvento"].",".getCartId())?>, this);" class="form-control text-center" value=<?php echo $biglietto["quantità"]?> min="1" max="6">
                    </td>
                    <td data-th="Subtotal" class="text-center"><?php echo ($biglietto["prezzo"] * $biglietto["quantità"]); $tot += ($biglietto["prezzo"] * $biglietto["quantità"])?></td>
                    <td class="actions" data-th="">
                        <button class="btn btn-danger btn-sm" onClick="deleteFromCart(<?php echo $biglietto["idEvento"].",".getCartId()?>)" ><i class="fa fa-2x fa-trash"></i></button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td><a href="index.php" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continua gli acquisti</a></td>
                    <td colspan="2" class="hidden-xs"></td>
                    <td class="hidden-xs text-center"><strong>Totale €<?php echo $tot?></strong></td>
                    <?php if($tot > 0) {?>
                    <td><a href="Checkout.php" class="btn over text-nowrap btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                    <?php }?>
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>