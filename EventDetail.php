<?php if(!isset($_SESSION)){session_start();}ob_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
<?php require 'template/head.php'; ?>
<link rel="stylesheet" href="css/eventDetail.css">
<script src="js/eventDetail.js"></script>
</head>

<body>
    <?php require 'template/nav.php'; ?>
    <?php
            $evento = $templateParams["evento"][0];
    ?>
    <div class="container">
        <div class="row mt-3">
            <div class="col-sm-10 flex-center">
                <div class="row">
                    <h1 class="text-center col-sm-12"><?php echo $evento["nome"] ?></h1>
                    <div class="col-sm-5">
                        <img class="p-2 align img-size" alt="..." src="<?php echo IMG_DIR.$evento["urlFoto"]; ?>">
                    </div>
                    <div class="col-sm-7">
                        <p>
                            <?php echo $evento["descrizione"] ?>
                        </p>
                        <p>Categoria evento: <?php echo $dbh->getCategoryById($evento["idCategoria"])[0]["nome"];?> </p>
                        <p>Data evento: <?php echo $evento["data"]?></p>
                        <p>Luogo evento: <?php echo $evento["luogo"]?></p>
                        <p>Città: <?php echo $evento["città"]?></p>
                        <p>Indirizzo:  <?php echo $evento["indirizzo"]?></p>
                        <p>Prezzo: <?php echo $evento["prezzo"]?>$</p>
                        <p>
                            Disponibilità:
                            <?php 
                                if($evento["numeroBiglietti"] - $evento["numeroBigliettiVenduti"] > 0){
                                    echo "<span class=\"badge badge-success ml-1 p-2 fs-badge \" >Disponibile</span>";
                                } else {
                                    echo "<span class=\"badge badge-danger ml-1 p-2 fs-badge \" >Non disponibile</span>";   
                                }
                            ?>
                        </p>
                        <form class="form-inline" name="addToCart-<?php echo $evento["id"]?>" id="addToCart" action="AddToCart.php">
                        <?php
                            if($evento["numeroBiglietti"] - $evento["numeroBigliettiVenduti"] > 0){
                        ?>
                        <div class="mt-3 col-12">
                            <div class="row">
                                    <a href="#" onClick="remove();" class="ml-0 col-3 btn btn-primary"><i class="fa fa-minus"></i></a>
                                    <input type="text" onChange="checkNumero();" class="col-3 text-center mt-2 form-control" name="numeroBiglietti" id="numeroBiglietti" value="0"/>
                                    <a href="#" onClick="add();" class=" btn col-3 btn-primary"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                            <button type="submit" class="btn text-center btn-primary btn-lg mt-3 ml-0">Aggiungi</button>                            
                        </form>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>