<?php
require_once 'bootstrap.php';

if(isManager() && !isAdmin()){
    $templateParams["eventi"] = $dbh->getEvents(getUserId());
} else if (isAdmin()){
    $templateParams["eventi"] = $dbh->getAllEvents();
}
require 'EventsList.php';
?>