<?php if(!isset($_SESSION)){session_start();require_once 'bootstrap.php';}ob_start();?>
<!DOCTYPE html>
<html lang="it">
<head>
    <?php require 'template/head.php'; ?>
    <link rel="stylesheet" href="css/insertUpdateEvent.css">
</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card col-sm-10">
                <div class="py-4 text-light text-center">
                    <div class="row">
                        <div class="col-2">
                            <a href="Events.php"><i class="fas text-center fa-2x fa-arrow-left text-white"></i></a>
                        </div>
                        <h3 class="text-center col-8">Inserisci/Aggiorna evento</h3>
                    </div>
                </div>
                <div class="row">
                </div>
                <div class="card-body mx-3">
                    <form name="insertEvent" id="insertEvent" method="POST" enctype="multipart/form-data" action="InsertUpdate.php">
                        <input type="hidden" name="id" value="<?php if(isset($_POST["idEvento"])){
                                echo $_POST["idEvento"];
                                $evento = $dbh->getEventById($_POST["idEvento"]);
                            }?>">
                        <div class="row">
                            <div class="input-group form-group">
                                <input type="text" class="form-control" placeholder="Titolo" name="titolo" <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["nome"]."\"";}?>>
                            </div>
                            <div class="input-group p-0 form-group">
                                <textarea class="form-control" rows="4" placeholder="descrizione"
                                    name="descrizione"><?php if(isset($_POST["idEvento"])){ echo $evento[0]["descrizione"];}?></textarea>
                            </div>
                            <div class="input-group p-0 col-sm-6 pr-sm-2 form-group">
                                <select class="form-control" name="categoria">
                                    <?php   $categories = $dbh->getCategories();
                                            foreach ($categories as $category):
                                                if(isset($_POST["idEvento"]) && $category["id"] == $evento[0]["idCategoria"]){
                                                    echo "<option value=".$category["id"]." selected>".$category["nome"]."</option>";
                                                } else {
                                                    echo "<option value=".$category["id"].">".$category["nome"]."</option>";
                                                }
                                            endforeach;
                                    ?>
                                </select>
                            </div>
                            <div class="input-group p-0 col-sm-6 form-group">
                                <input type="datetime-local" class="form-control" name="dataEvento"
                                <?php if(isset($_POST["idEvento"])){ echo "value=\"".date("Y-m-d\TG:i", strtotime($evento[0]["data"]))."\"";}?>>
                            </div>
                            <div class="input-group p-0 pr-sm-2 col-sm-6 form-group">
                                <input type="text" class="form-control" placeholder="Citta" name="citta"
                                <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["città"]."\"";}?>>
                            </div>
                            <div class="input-group p-0 col-sm-6 form-group">
                                <input type="text" class="form-control" placeholder="Luogo" name="luogo"
                                <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["luogo"]."\"";}?>>
                            </div>
                            <div class="input-group p-0 form-group">
                                <input type="text" class="form-control" placeholder="Indirizzo" name="indirizzo"
                                <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["indirizzo"]."\"";}?>>
                            </div>
                            <div class="input-group pr-sm-2 col-sm-6 p-0 form-group">
                                <input type="number" class="form-control" min="1" placeholder="Numero biglietti"
                                    name="biglietti" <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["numeroBiglietti"]."\"";}?>>
                            </div>
                            <div class="input-group col-sm-6 p-0 form-group">
                                <input type="number" class="form-control" min="1" placeholder="Prezzo" aria-describedby="valuta" name="prezzo"
                                <?php if(isset($_POST["idEvento"])){ echo "value=\"".$evento[0]["prezzo"]."\"";}?>>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="valuta">€</span>
                                </div>
                            </div>
                            <div class="input-group p-0 form-group">
                                <label for="image" class="text-light">Immagine: </label>
                                <input class="col-8 form-control-file text-light" type="file" name="image" id="image">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Inserisci" class="btn checkout_btn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>