<?php
require_once 'bootstrap.php';
$loc = "LoginForm.php";
if(strlen($_POST["username"]) != 0 && strlen($_POST["p"]) != 0  && $_POST["p"] != "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"){
    $username=$_POST["username"];
    $password=$_POST["p"];
    $user = $dbh->login($username);
    if(isset($user[0])){
        $user = $user[0];
        $password=hash('sha512', $password.$user["seed"]);
        if($user["password"] == $password){
            registerLoggedUser($user);
            require 'index.php';
            //header("location: ./index.php");
            exit;
        } else {//Passoword non corretta
            error(1,$loc);
        }
    } else {//Utente non esistente
        error(2,$loc);
    }
} else {//Campi mancanti
    error(3,$loc);
}
?>