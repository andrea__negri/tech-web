<?php
require_once 'bootstrap.php';

if(isManager()){
    $templateParams["notificheOrg"] = $dbh->getManagerNotifications(getUserId());
}
$templateParams["notifiche"] = $dbh->getUserNotifications(getUserId());
require 'NotificationsList.php';
?>