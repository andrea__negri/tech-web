<?php if(!isset($_SESSION)){session_start();}ob_start();?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <?php require 'template/head.php'; ?>
        <link rel="stylesheet" href="css/notificationList.css">
        <script src="js/notification.js"></script>
    </head>

<body>
    <?php require 'template/nav.php'; ?>
    <div class="container">
        <table id="notification" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th id = "Evento" class="w-40">Evento</th>
                    <th id = "Descrizione" class="w-30">Descrizione</th>
                    <th id = "Azioni" class="w-10"></th>
                </tr>
            </thead>
            <tbody>
                <?php if(isset($templateParams["notificheOrg"])){
                    foreach($templateParams["notificheOrg"] as $notifica):?>
                    <tr>
                        <td data-th="Evento">
                            <div class="row">
                                <div class="col-sm-2 hidden-xs"><img src=<?php echo IMG_DIR.$notifica["urlFoto"]?> alt="..." class="img-responsive img-size" /></div>
                                <div class="col-sm-10">
                                    <a href="Detail.php?id=<?php echo $notifica["idEvento"]?>"><h4 class="nomargin"><?php echo $notifica["nome"]?></h4></a>
                                </div>
                            </div>
                        </td>
                        <td data-th="Descrizione"><?php echo $notifica["descrizione"] ?></td>
                        <td data-th="Azioni">
                            <a class="ml-3"><i id="bookmarkOrg-<?php echo $notifica["id"]?>" onClick="read(this)" title="Marca come letta" class="
                            <?php   if($notifica["letto"] == 0){
                                        echo "far fa-2x fa-bookmark text-danger";
                                    } else {
                                        echo "fas fa-2x fa-bookmark text-danger";
                                    }
                            ?>"></i></a>
                        </td>
                    </tr>
                <?php endforeach;}?>
                <?php if(isset($templateParams["notifiche"])){
                    foreach($templateParams["notifiche"] as $notifica):?>
                    <tr>
                        <td data-th="Evento">
                            <div class="row">
                                <div class="col-sm-2 hidden-xs"><img src=<?php echo IMG_DIR.$notifica["urlFoto"]?>  alt="..." class="img-responsive img-size" /></div>
                                <div class="col-sm-10">
                                    <a href="Detail.php?id=<?php echo $notifica["idEvento"]?>"><h4 class="nomargin"><?php echo $notifica["nome"]?></h4></a>
                                </div>
                            </div>
                        </td>
                        <td data-th="Descrizione"><?php echo $notifica["descrizione"] ?></td>
                        <td data-th="Azioni">
                            <a class="ml-3"><i id="bookmark-<?php echo $notifica["id"]?>" onClick="read(this)" title="Marca come letta" class="
                            <?php   if($notifica["letto"] == 0){
                                        echo "far fa-2x fa-bookmark text-danger";
                                    } else {
                                        echo "fas fa-2x fa-bookmark text-danger";
                                    }
                            ?>"></i></a>
                        </td>
                    </tr>
                <?php endforeach;}?>

            </tbody>
        </table>
    </div>
</body>
</html>