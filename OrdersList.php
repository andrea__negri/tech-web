<?php if(!isset($_SESSION)){session_start();}ob_start();?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <?php require 'template/head.php'; ?>
        <link rel="stylesheet" href="css/ordersList.css">
    </head>

<body>
    <?php require 'template/nav.php'; ?>
    <div class="container">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th id = "Evento" class="w-30">Evento</th>
                    <th id = "Proprietario" class="w-10">Proprietario Carta</th>
                    <th id = "Prezzo" class="w-10">Prezzo (unitario)</th>
                    <th id = "Quantità" class="w-10">Quantità</th>
                    <th id = "Data" class="w-10">Data ordine</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($templateParams["ordini"] as $ordine): ?>
                <tr>
                    <td data-th="Evento">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs"><img src="<?php echo IMG_DIR.$ordine["urlFoto"]?>" alt="..." class="img-responsive img-size" /></div>
                            <div class="col-sm-10">
                                <h4 class="nomargin"><a href="Detail?id=<?php echo $ordine["id"]?>"><?php echo $ordine["nome"]?></a></h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Proprietario"><?php echo $ordine["proprietarioCarta"]?></td>
                    <td data-th="Prezzo"><?php echo $ordine["prezzo"]?> €</td>
                    <td data-th="Quantità"><?php echo $ordine["quantità"]?></td>
                    <td data-th="Data"><?php echo $ordine["data"]?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</body>
</html>