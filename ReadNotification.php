<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("HTTP/1.0 500 User non loggato");
    exit;
}
else {
    if(!isset($_POST["typeOfRead"]) || !isset($_POST["idRow"]) || !isset($_POST["read"])){
        header("HTTP/1.0 500 Parametri non validi");
        exit;
    } else {
        $tipolettura = $_POST["typeOfRead"];
        $notification["tabella"] = $tipolettura == 1 ? "notificheorganizzatore" : "notificheutente";
        $notification["riga"] = $_POST["idRow"];
        $notification["letta"] = $_POST["read"];
        if($dbh->readNotification($notification)){
            header("HTTP/1.0 200 Ok");
            exit;
        } else {
            header("HTTP/1.0 500 Errore durante la modifica");
            exit;
        }
    }
}
header("HTTP/1.0 500 User non loggato 2");
exit;
?>