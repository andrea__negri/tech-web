<?php
require_once 'bootstrap.php';
$loc = "SubscribeForm.php";
$organizzatore = isset($_POST["organizzatore"]) ? 1 : 0;
if( strlen($_POST["nome"]) != 0 && strlen($_POST["cognome"]) != 0 && strlen($_POST["email"]) != 0 &&
    strlen($_POST["p"]) != 0 && strlen($_POST["p2"]) != 0 &&
    strlen($_POST["data"]) != 0){
    if($_POST["p"] == $_POST["p2"]){
        $user["nome"] = $_POST["nome"];
        $user["cognome"] = $_POST["cognome"];
        $user["email"] = $_POST["email"];
        $user["password"] = $_POST["p"];
        $user["dataNascita"] = date("Y-m-d", strtotime($_POST["data"]));
        $user["organizzatore"] = $organizzatore;
        $randomSeed = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $user["seed"] = $randomSeed;
        $user["password"] = hash('sha512', $user["password"].$randomSeed);
        if($dbh->checkEmail($user["email"])){
            if($dbh->register($user)){
                justRegistered();
                require 'index.php';
                //header("location: ./index.php");
                exit;
            } else {//Registrazione fallita
                error(1,$loc);
            }
        } else {//Email già registrata
            error(2,$loc);
        }
    } else {//Passoword non corrispondenti
        error(3,$loc);
    }
} else {//Campi mancanti
    error(4,$loc);
}
?>