function modifyQty(idEv, idCar, input){
    if(!checkNumero1(input)){
        return;
    }
    let qty = parseInt(input.value);
    let modified = $.post("RemoveFromCart.php", {idCarrello: idCar, idEvento: idEv, quantità: qty});
    modified.done(function(data){
        toastr.success("Modifica avvenuta!");
        window.location.reload();
    }).fail(function(jqXHR, textStatus, error){
        toastr.error(error);
    });
}

function deleteFromCart(idEv, idCar){
    if(!confirm("Sei sicuro di voler rimuovere l'evento dal carrello?")){
        return;
    }
    let removed = $.post("DeleteFromCart.php", {idCarrello: idCar, idEvento: idEv});
    removed.done(function(data){
        toastr.success("Evento rimosso!");
        window.location.reload();
    }).fail(function(jqXHR, textStatus, error){
        toastr.error(error);
    });
}

function checkNumero1(input){
    let qty = parseInt(input.value);

    if(isNaN(qty)){
        toastr.error("Le stringhe non sono ammesse");
        input.value = 1;
        return false;
    }
    if(qty < 0){
        toastr.error("Hai inserito un numero negativo");
        input.value = 1;
        return false;
    }
    if(qty == 0){
        toastr.error("Non puoi acquistare 0 biglietti");
        input.value = 1;
        return false;
    }
    if(qty > 6){
        toastr.error("Non puoi aggiungere più di 6 biglietti");
        input.value = 6;
        return false;
    }
    return true;
}