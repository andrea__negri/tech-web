$(document).ready(function(){
    $("#buyTicket").submit(function(e){
        e.preventDefault();
        let form = $( this );
        let url = form.attr( "action" );
        if(!checkData(form)){
            return;
        }
        name = form.find("input[name='proprietario']").val();
        card = form.find("input[name='numeroCarta']").val();
        cvv = form.find("input[name='cvv']").val();
        let order = $.post(url, {proprietario: name, carta: card, cvv: cvv});
        order.done(function(data){
            toastr.success("Ordine effettuato!");
            window.location.replace("index.php");
        }).fail(function(jqXHR, textStatus, error){
            toastr.error(error);
        });
    });
})

function checkData(form){
    let name = form.find("input[name='proprietario']").val();
    if(name.trim() == ""){
        toastr.error("Il nominativo non può essere vuoto!");
        return false;
    }
    let card = form.find("input[name='numeroCarta']").val();
    if(isNaN(card) || card == ""){
        toastr.error("Il numero di carta non può contenere lettere o essere lasciato vuoto!");
        return false;
    }
    let cvv = form.find("input[name='cvv']").val();
    if(isNaN(cvv) || cvv == ""){
        toastr.error("Il cvv della carta non può contenere lettere o essere lasciato vuoto!");
        return false;
    }
    return true;
}