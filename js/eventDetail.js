$(document).ready(function(){
    $("#addToCart").submit(function(e){
        e.preventDefault();
        if(!checkNumero()){
            return;
        }
        let form = $( this );
        let num = form.find("input[name='numeroBiglietti']").val();
        let id = form.attr("name");
        id = id.split("-");
        id = id[1];
        let url = form.attr( "action" );
        let added = $.post(url, {numeroBiglietti: num, idEvento: id});
        added.done(function(data){
            toastr.success("Biglietti aggiunti al carrello!");
        }).fail(function(jqXHR, textStatus, error){
            toastr.error(error);
        });
    });
})

function checkNumero(){
    let current = parseInt($("#numeroBiglietti").val());

    if(isNaN(current)){
        toastr.error("Le stringhe non sono ammesse");
        $("#numeroBiglietti").val('0');
        return false;
    }
    if(current < 0){
        toastr.error("Hai inserito un numero negativo");
        $("#numeroBiglietti").val('0');
        return false;
    }
    if(current == 0){
        toastr.error("Non puoi acquistare 0 biglietti");
        $("#numeroBiglietti").val('0');
        return false;
    }
    if(current > 6){
        toastr.error("Non puoi aggiungere più di 6 biglietti");
        $("#numeroBiglietti").val('6');
        return false;
    }
    return true;
}

function add(){
    let numeroBiglietti = parseInt($("#numeroBiglietti").val(), 10);

    if(numeroBiglietti >= 6){
        toastr.error("Non puoi aggiungere più di 6 biglietti");
    } else {
        numeroBiglietti = numeroBiglietti + 1;
    }
    document.getElementById("numeroBiglietti").value = numeroBiglietti;
}

function remove(){
    let numeroBiglietti = parseInt($("#numeroBiglietti").val(), 10);

    if(numeroBiglietti != 0){
        numeroBiglietti = numeroBiglietti - 1;
    }
    else{
        toastr.error("Non puoi andare sotto zero");
    }

    document.getElementById("numeroBiglietti").value = numeroBiglietti;
}
