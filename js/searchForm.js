let range = document.getElementById("rangeFilter");

range.oninput = function () {
    range.setAttribute('title', $("#rangeFilter").val());
}

function checkDate() {
    let data1 = new Date(document.getElementById("dataDal").value);
    let data2 = new Date(document.getElementById("dataAl").value);

    if (data1 != null && data2 != null && data1 > data2) {
        toastr.error("date errate");
        return false;
    }

    return true;
}