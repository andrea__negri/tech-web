<header>
    <a href="index.php"><h1 class="text-center mb-3">E - Buy</h1></a>
    <nav class="navbar navbar-dark mb-4">
        <div class="row w-100">
            <div class="col-sm-2 col-md-2 text-center py-2">
                <button class="navbar-toggler second-button" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent23" aria-controls="navbarSupportedContent23" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <div class="animated-icon2"><span></span><span></span><span></span><span></span></div>
                </button>
            </div>
            <div class="col-sm-7 col-md-7 nav-desktop">
                <form class="form-inline ml-auto mr-auto" method="POST" action="Search.php">
                    <div class="md-form m-0 w-100">
                        <input class="form-control w-75" type="text" placeholder="Search" aria-label="Search" name="searching">
                        <i class="fas fa-lg fa-search text-white ml-3 icon-allign" aria-hidden="true"></i>
                    </div>
                </form>
            </div>
            <div class="col-sm-3 col-md-3 nav-desktop">
                <div class="mr-3 text-right">
                    <div class="row">

                        <?php
                        if(!isUserLoggedIn()) {
                            echo "<div class=\"col-sm-4\"><a href=\"LoginForm.php\"><i class=\"fas fa-user-circle text-white icon-allign icon-user-allign\"></i></a></div>";
                        } else {
                            echo "  <div class=\"dropdown col-sm-4\">
                                        <button class=\"btn btn-link p-0\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                        <i class=\"fas fa-2x fa-user-circle text-white icon-allign\"></i>
                                        </button>
                                        
                                        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                                            <a class=\"dropdown-item\" href=\"Orders.php\">Ordini</a>
                                            <a class=\"dropdown-item\" href=\"Notifications.php\">Notifiche</a>";
                                            if(isManager()){
                                                echo "<a class=\"dropdown-item\" href=\"Events.php\">Eventi</a>";                                               
                                            }
                                            echo "<a class=\"dropdown-item text-primary\" href=\"Logout.php\">Logout</a>
                                        </div>
                                    </div>
                                ";
                            }
                        ?>
                        <a href="Cart.php"><i class="fas p-0 col-sm-4 fa-shopping-cart text-white icon-allign cart-allign"></i></a>
                    </div>
                </div>
            </div>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent23">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item nav-mobile">
                        <div class="col-sm-7">
                            <form class="form-inline ml-auto mr-auto" method="POST" action="Search.php">
                                <div class="md-form m-0 w-100">
                                    <div class="row">
                                        <input class="form-control col-8" type="text" placeholder="Search" aria-label="Search" name="searching">
                                        <i class="fas col-2 fa-lg fa-search text-white mt-2 ml-3 icon-allign" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </form>
                        </div>  
                    </li>
                    <?php
                    if(!isUserLoggedIn()) {
                            echo "
                            <li class=\"nav-item nav-mobile\">
                                <div class=\"row\">
                                    <a href=\"LoginForm.php\"><i class=\"fas col-2 fa-2x fa-user-circle text-white icon-allign mt-2 ml-2\"></i></a>
                                    <a href=\"Cart.php\"><i class=\"fas col-2 pt-3 fa-2x fa-shopping-cart text-white icon-allign\"></i></a> 
                                </div>
                            </li>
                            ";
                        }else{
                            echo "
                            <li class=\"nav-item dropdown row nav-mobile\">
                                <a class=\"nav-link\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                <i class=\"fas fa-2x ml-3 fa-user-circle text-white icon-allign\"></i>
                                </a>
                                <div class=\"col-2\">
                                    <a href=\"Cart.php\"><i class=\"fas fa-2x pt-3 fa-shopping-cart text-white icon-allign\"></i></a>
                                </div>
                                <div class=\"dropdown-menu ml-3\" aria-labelledby=\"navbarDropdown\">
                                    <a class=\"dropdown-item\" href=\"Orders.php\">Ordini</a>
                                    <a class=\"dropdown-item\" href=\"Notifications.php\">Notifiche</a>";
                                    if(isManager()){
                                        echo "<a class=\"dropdown-item\" href=\"Events.php\">Eventi</a>";                                               
                                    }
                                    echo "<a class=\"dropdown-item text-primary\" href=\"Logout.php\">Logout</a>
                                </div>
                            </li>
                            ";
                    }
                    ?>

                    <li class="nav-item active">
                        <a class="nav-link text-center" href="index.php">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-center" href="Search.php">Ricerca Avanzata</a>
                    </li>                    
                </ul>
            </div>
        </div>
    </nav>
</header>
