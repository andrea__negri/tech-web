<?php
function registerLoggedUser($user){
    $_SESSION["iduser"] = $user["iduser"];
    $_SESSION["username"] = $user["username"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["organizzatore"] = $user["organizzatore"];
    $_SESSION["amministratore"] = $user["amministratore"];
    $_SESSION["idcarrello"] = $user["idcarrello"];
    $_SESSION["justlogged"] = 1;
}

function logout(){
    $_SESSION = array();
    $_POST = array();
    session_destroy();
    header("location: ./index.php");
}

function getUserId(){
    if(isUserLoggedIn()){
        return $_SESSION["iduser"];
    }
}

function getCartId(){
    if(isUserLoggedIn()){
        return $_SESSION["idcarrello"];
    }
}

function isAdmin(){
    return (isset($_SESSION["amministratore"]) && $_SESSION["amministratore"] == 1);
}

function isManager(){
    return (isset($_SESSION["organizzatore"]) && $_SESSION["organizzatore"] == 1);
}

function isUserLoggedIn(){
    return !empty($_SESSION["iduser"]);
}

function isJustLogged(){
    if(isset($_SESSION["justlogged"]) && $_SESSION["justlogged"] == 1){
        return true;
    }
    return false;
}

function logged(){
    $_SESSION["justlogged"] = 0;
}

function justRegistered(){
    $_SESSION["justregistered"] = 1;
}

function registered(){
    $_SESSION["justregistered"] = 0;
}

function isJustRegistered(){
    if(isset($_SESSION["justregistered"]) && $_SESSION["justregistered"] == 1){
        return true;
    }
    return false;
}

function error($err, $location){
    session_unset();
    $_POST = array();
    require $location;
    $_SESSION["error"] = $err;
    header("location: ./".$location);
    exit;
}
?>